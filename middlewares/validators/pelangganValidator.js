const { barang, pelanggan, pemasok, transaksi } = require("../../models");
const mongoose = require("mongoose");
const { default: validator } = require("validator");

module.exports.getOne = async (req, res, next) => {
  let error = [];
  let findData = await pelanggan.findOne({
    where: { _id: req.params.id },
  });

  if (!mongoose.Types.ObjectId.isValid(req.params.id)) {
    error.push("params is not hexadecimal");
  }

  if (!findData) {
    error.push("data is not found");
  }

  if (error.length > 0) {
    return res.status(400).json({
      message: error.join(", "),
    });
  }

  next();
};

module.exports.create = async (req, res, next) => {
  let error = [];

  // Check Empty input
  if (!validator.isEmpty(req.body.name)) {
    error.push("Name is empty");
  }

  // Check is nama contain only alphabet
  if (!validator.isAlpha(validator.blacklist(req.body.nama, " "))) {
    error.push("Nama is not alphabet");
  }

  if (error.length > 0) {
    return res.status(400).json({
      message: error.join(", "),
    });
  }

  req.body.nama = req.body.nama;
  req.body.photo = req.body.image;

  next();
};
