const passport = require("passport");
const LocalStrategy = require("passport-local");
const bcrypt = require("bcrypt"); // Import bcrypt
const JWTstrategy = require("passport-jwt").Strategy; // Import JWT Strategy
const ExtractJWT = require("passport-jwt").ExtractJwt; // Import ExtractJWT
const { user } = require("../../models");

exports.signup = (req, res, next) => {
  passport.authenticate("signup", { session: false }, (err, user, info) => {
    if (err) {
      return res.status(500).json({
        message: "Internal Server Error",
        error: err,
      });
    }
    if (!user) {
      return res.status(401).json({
        status: "Error",
        message: info.message,
      });
    }
    req.user = user;
    next();
  })(req, res, next);
};

passport.use(
  "signup",
  new LocalStrategy(
    {
      usernameField: "email",
      passwordField: "password",
      passReqToCallback: true,
    },
    async (req, email, password, done) => {
      try {
        let userSignUp = await user.create(req.body);

        return done(null, userSignUp, {
          message: "User can be created",
        });
      } catch (e) {
        console.log(e);
        return done(null, false, {
          message: "User can't be created",
        });
      }
    }
  )
);

exports.signin = (req, res, next) => {
  passport.authenticate("signin", { session: false }, (err, user, info) => {
    if (err) {
      return res.status(500).json({
        message: "Internal Server Error",
        error: err,
      });
    }
    if (!user) {
      return res.status(404).json({
        status: "Error",
        message: info.message,
      });
    }
    req.user = user;
    next();
  })(req, res, next);
};

passport.use(
  "signin",
  new LocalStrategy(
    {
      usernameField: "email",
      passwordField: "password",
      passReqToCallback: true,
    },
    async (req, email, password, done) => {
      try {
        let userSignIn = await user.findOne({ email });
        if (!userSignIn) {
          return done(null, false, {
            message: "Email not found",
          });
        }

        let validate = await bcrypt.compare(password, userSignIn.password);
        if (!validate) {
          return done(null, false, {
            message: "Password not found",
          });
        }

        return done(null, userSignIn, {
          message: "User can sign in",
        });
      } catch (e) {
        console.log(e);
        return done(null, false, {
          message: "User can't sign in",
        });
      }
    }
  )
);

exports.admin = (req, res, next) => {
  passport.authenticate("admin", (err, user, info) => {
    if (err) {
      return res.status(500).json({
        message: "Internal Server Error",
        error: err,
      });
    }
    if (!user) {
      return res.status(403).json({
        status: "Error",
        message: info.message,
      });
    }
    req.user = user;
    next();
  })(req, res, next);
};

passport.use(
  "admin",
  new JWTstrategy(
    {
      secretOrKey: process.env.JWT_SECRET,
      jwtFromRequest: ExtractJWT.fromAuthHeaderAsBearerToken(),
    },
    async (token, done) => {
      try {
        const userLogin = await user.findOne({ _id: token.user.id });

        if (userLogin.role.includes("admin")) {
          return done(null, token.user);
        }
        done(null, false, {
          message: "You're not authorized",
        });
      } catch (e) {
        done(null, false, {
          message: "You're not authorized",
        });
      }
    }
  )
);

exports.user = (req, res, next) => {
  passport.authenticate("user", (err, user, info) => {
    if (err) {
      return res.status(500).json({
        message: "Internal Server Error",
        error: err,
      });
    }
    if (!user) {
      return res.status(403).json({
        status: "Error",
        message: info.message,
      });
    }
    req.user = user;
    next();
  })(req, res, next);
};

passport.use(
  "user",
  new JWTstrategy(
    {
      secretOrKey: process.env.JWT_SECRET,
      jwtFromRequest: ExtractJWT.fromAuthHeaderAsBearerToken(),
    },
    async (token, done) => {
      try {
        const userLogin = await user.findOne({ _id: token.user.id });

        if (userLogin.role.includes("user")) {
          return done(null, token.user);
        }
        done(null, false, {
          message: "You're not authorized",
        });
      } catch (e) {
        done(null, false, {
          message: "You're not authorized",
        });
      }
    }
  )
);
