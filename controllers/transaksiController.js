const { barang, pelanggan, pemasok, transaksi } = require("../models");

class TransaksiController {
  async getAll(req, res) {
    try {
      let data = await transaksi.find();

      if (data.length === 0) {
        return res.status(404).json({
          message: "Transaksi Not Found!",
        });
      }
      return res.status(200).json({
        message: "Success",
        data,
      });
    } catch (e) {
      console.log(e);
      return res.status(500).json({
        message: "Internal Server Error",
        error: e,
      });
    }
  }
  async getOne(req, res) {
    try {
      let data = await transaksi.findOne({ _id: req.params.id });

      if (!data) {
        return res.status(500).json({
          message: "Transaksi Not Found!",
        });
      }
      return res.status(200).json({
        message: "Success",
        data,
      });
    } catch (e) {
      return res.status(500).json({
        message: "Internal Server Error",
        error: e,
      });
    }
  }
  async create(req, res) {
    try {
      let data = await transaksi.create(req.body);
      return res.status(200).json({
        message: "Success",
        data,
      });
    } catch (e) {
      console.log(e);
      return res.status(500).json({
        message: "Internal Server Error",
        error: e,
      });
    }
  }
  async update(req, res) {
    try {
      let data = await transaksi.findOneAndUpdate(
        {
          _id: req.params.id,
        },
        req.body,
        {
          new: true,
        }
      );

      return res.status(201).json({
        message: "Success",
        data,
      });
    } catch (e) {
      return res.status(500).json({
        message: "Internal Server Error",
        error: e,
      });
    }
  }
  async delete(req, res) {
    try {
      await transaksi.delete({ _id: req.params.id });

      return res.status(200).json({
        message: "Success",
      });
    } catch (e) {
      return res.status(500).json({
        message: "Internal Server Error",
        error: e,
      });
    }
  }
}

module.exports = new TransaksiController();
