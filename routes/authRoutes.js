const express = require("express"); //impoert express
const passport = require("passport"); //import passport
const router = express.Router();
//import validator

//import controller
const authController = require("../controllers/authController");
const authValidatior = require("../middlewares/validators/authValidator");
//import auth(middlewares)
const auth = require("../middlewares/auth");

router.post(
  "/signup",
  authValidatior.signup,
  auth.signup,
  authController.getToken
);
router.post(
  "/signin",
  authValidatior.signin,
  auth.signin,
  authController.getToken
);

module.exports = router;
