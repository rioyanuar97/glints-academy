const express = require("express");

//import validator
const pemasokValidator = require("../middlewares/validators/pemasokValidator");
//import controller
const pemasokController = require("../controllers/pemasokController");
//make router
const router = express.Router();

//get all data
router.get("/", pemasokController.getAll);
router.get("/:id", pemasokValidator.getOne, pemasokController.getOne);
router.post("/", pemasokValidator.create, pemasokController.create);
router.put("/:id", pemasokValidator.update, pemasokController.update);
router.delete("/:id", pemasokValidator.delete, pemasokController.delete);

module.exports = router;
